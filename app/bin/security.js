'use strict';

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('./../models/user');

export class Security {
    
    constructor() {
        
        this.passport = passport;
        
        /**
         * Serialize user
         */
        this.passport.serializeUser((user, done) => {
            done(null, user.id);
        });

        /**
         * Deserialize user
         */
        this.passport.deserializeUser((id, done) => {
            User.findById(id, (err, user) => {
                done(err, user);
            });
        });

        /**
         * Local sign up strategy
         */
        this.passport.use('local-signup', new LocalStrategy({
                usernameField : 'email',
                passwordField : 'password',
                passReqToCallback : true
            },
            function(req, email, password, done) {

                User.findOne({ 'local.email' : email }, (err, user) => {
    
                    if (err) {
                        console.log(err);
                        return done(err);
                    }
    
                    if (user) {
                        return done(null, false, req.flash('signup-message', 'That email is already taken.'));
                    } else {
    
                        var newUser = new User();
    
                        newUser.local.email = email;
                        newUser.local.password = newUser.generateHash(password);
                        newUser.local.username = req.body.username;
    
                        newUser.save((err) => {
                            if (err) {
                                throw err;
                            }
                            return done(null, newUser);
                        });
                    }
                });
    
            }
        ));

        /**
         * Local login strategy
         */
        this.passport.use('local-login', new LocalStrategy({
                usernameField : 'email',
                passwordField : 'password',
                passReqToCallback : true
            },
            (req, email, password, done) => {
    
                User.findOne({ 'local.email' : email }, (err, user) => {
    
                    if (err) {
                        return done(err);
                    }
    
                    if (!user) {
                        return done(null, false, req.flash('login-message', 'No user found.'));
                    }
    
                    if (!user.validPassword(password)) {
                        return done(null, false, req.flash('login-message', 'Oops! Wrong password.'));
                    }
    
                    return done(null, user);
                });
    
            }
        ));
    }
}