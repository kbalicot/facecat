'use strict';

import {DefaultRouter} from './../routers/default';
import {Security} from './security';

var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jade = require('jade');
var cors = require('cors');
var params = require('./../config/parameters.json');
var flash = require('connect-flash');
var session = require('express-session');
var i18n = require('i18next');

export class Express {

    constructor() {

        let router = express.Router();

        this.app = express();
        this.config = params[process.env.NODE_ENV];
        this.security = new Security();
        
        /**
         * Dependencies configuration
         */
        i18n.init({
            resGetPath: 'build/config/locales/__lng__/__ns__.json'
        });
        i18n.registerAppHelper(this.app);
        
        /**
         * Routes
         */
        new DefaultRouter(router, this.security.passport);

        /**
         * Statics
         */
        this.app.use('/js', express.static('./public/js'));
        this.app.use('/app', express.static('./public/js/app'));
        this.app.use('/css', express.static('./public/css'));
        this.app.use('/img', express.static('./public/img'));
        this.app.use('/lib', express.static('./public/lib'));
        this.app.use('/html', express.static('./public/html'));

        /**
         * Configuration
         */
        this.app.use(i18n.handle);
        this.app.use(cookieParser());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(cors());
        this.app.use(flash());
        this.app.use(session({ resave: true, saveUninitialized: true, secret: 'ilovescotchscotchyscotchscotch' }));
        this.app.use(this.security.passport.initialize());
        this.app.use(this.security.passport.session());

        this.app.set('view engine', 'jade');
        this.app.set('views', './public/views');
        
        /**
         * Locals
         */
        //app.locals.moment = moment;

        this.app.use(this.config.application.path, router);
    }
}