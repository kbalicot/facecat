'use strict';

var mongoose = require('mongoose');

export class Database {
    
    connection(host, database, user = null, password = '') {
        let uri = 'mongodb://';
        
        if (user != null) {
            uri += `${user}:${password}@`;
        }
        
        this.connection = mongoose.connect(`${uri}${host}/${database}`);
    }
}