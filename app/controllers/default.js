'use strict';

export class DefaultController {

    /**
     * Index action
     * @param req
     * @param res
     */
    index(req, res) {
        
        if (req.isAuthenticated()) {
            res.redirect('/home', { user: req.user });
        } else {
            res.render('index', { message: req.flash('login-message') });
        }
    }
    
    /**
     * Home action
     * @param req
     * @param res
     */
    home(req, res) {
        res.render('home', { user: req.user });
    }
    
    /**
     * Sign up render view
     * @param req
     * @param res
     */
    signUp(req, res) {
        res.render('sign-up', { message: req.flash('signup-message') });
    };
    
    /**
     * Logout render view
     * @param req
     * @param res
     */
    logout(req, res) {
        req.logout();
        res.redirect('/');
    };
}