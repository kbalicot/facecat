'use strict';

import {Express} from './bin/express';
import {Database} from './bin/database';

class Bootstrap extends Express {

    constructor() {
        super();
        
        if (this.config.db != undefined) {
            this.db = new Database();
            
            this.db.connection(
                this.config.db.host,
                this.config.db.database,
                this.config.db.user,
                this.config.db.password
            );
        }
    }

    run(port = 8080) {
        this.app.listen(port);
        console.log(`Server launched on port ${port} on ${process.env.NODE_ENV} environment`);
    }
}

module.exports = function() {
    return new Bootstrap();
};