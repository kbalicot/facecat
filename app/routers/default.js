'use strict';

import {DefaultController} from './../controllers/default';
import {ArticleController} from './../controllers/article';

export class DefaultRouter {

    /**
     * @param router
     */
    constructor(router, security) {

        let controller = new DefaultController();
        let articleController = new ArticleController();

        router.route('/')
            .get(controller.index)
            .post(security.authenticate('local-login', {
                successRedirect : '/home',
                failureRedirect : '/',
                failureFlash : true
            }))
        ;
        
        router.route('/home')
            .get(this.isLoggedIn, controller.home)
        ;
        

        router.route('/signup')
            .get(controller.signUp)
            .post(security.authenticate('local-signup', {
                successRedirect : '/home',
                failureRedirect : '/signup',
                failureFlash : true
            }))
        ;

        router.get('/logout', controller.logout);
        
        router.route('/articles')
            .post(this.isLoggedIn, articleController.create)
        ;
    }
    
    isLoggedIn(req, res, next) {

        if (req.isAuthenticated()) {
            return next();
        }

        res.redirect('/');
    }
}