'use strict';

var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = new mongoose.Schema({
    local: {
        username: String,
        email: String,
        password: String
    },
    coutry: { type: String, default: 'fr' }
});

/**
 * Hash password
 * @param password
 * @returns {*}
 */
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/**
 * Check is valid password
 * @param password
 * @returns {*}
 */
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);