'use strict';

var gulp = require('gulp');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var copy = require('gulp-copy');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');

gulp.task('app', function() {
    gulp.src('app/**/*.js')
        .pipe(babel())
        .pipe(gulp.dest('build'))
    ;
});

gulp.task('config', function() {
    gulp.src('app/config/**/*.json')
        .pipe(gulp.dest('build/config'))
    ;
});

gulp.task('less', function() {
    gulp.src('public/less/*.less')
        .pipe(less())
        .pipe(minifyCSS())
        .pipe(gulp.dest('public/css'))
    ;
});

gulp.task('bower', function() {
    gulp.src('bower_components/**/*.*')
        .pipe(gulp.dest('public/lib'))
    ;
});

gulp.task('watch', function () {
    gulp.watch(['./app/**/*.js'], ['app']);
    gulp.watch(['./app/config/**/*.json'], ['config']);
    gulp.watch(['./public/less/*.less'], ['less']);
});

gulp.task('default', ['app', 'config', 'less', 'bower']);
gulp.task('dev', ['default', 'watch']);