var app = angular.module('facecat', ['ngAnimate', 'ui.bootstrap']);

app.controller('navCtrl', function($modal) {
    var self = this;
    
    self.write = function() {
        $modal.open({
          animation: true,
          controller: 'meowCtrl',
          controllerAs: 'm',
          templateUrl: '../html/partials/modals/write.html',
          size: 'lg'
        });
    };
});

app.controller('meowCtrl', function($http) {
    var self = this;
    
    self.write = function(something, callback) {
        if (something != undefined) {
            $http
                .post('/articles', { text: something })
                .success(function() {
                    callback();
                })
            ;
        }
    };
});