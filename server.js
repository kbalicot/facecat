'use strict';

if (process.env.NODE_ENV == undefined) {
    process.env.NODE_ENV = 'dev';
}

var server = require('./build/bootstrap')();

server.run();